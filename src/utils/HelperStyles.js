import {StyleSheet} from 'react-native';

export const justView = (styleLabel, styleValue) => {
  return {
    [styleLabel]: styleValue,
  };
};

export const flexDirection = flexDirectionValue => {
  return {
    flexDirection: flexDirectionValue,
  };
};

// Flex Styles
export const flex = flexValue => {
  return {
    flex: flexValue,
  };
};

export const flexGrow = flexValue => {
  return {
    flexGrow: flexValue,
  };
};

// Image Styles
export const imageView = (heightValue, widthValue) => {
  return {
    height: heightValue,
    width: widthValue,
  };
};

export const textView = (
  sizeValue,
  weightValue,
  colorValue,
  alignValue,
  textTransformValue,
) => {
  return {
    color: colorValue,
    //   fontFamily: 'Proxima Nova',
    fontSize: sizeValue,
    fontWeight: weightValue,
    textAlign: alignValue,
    textTransform: textTransformValue,
  };
};

// Other Styles
export const justifyContentCenteredView = justifyContentValue => {
  return {
    justifyContent: justifyContentValue,
    alignItems: 'center',
  };
};

export const margin = (marginHorizontalValue, marginVerticalValue) => {
  return {
    marginHorizontal: marginHorizontalValue,
    marginVertical: marginVerticalValue,
  };
};

export const padding = (paddingHorizontalValue, paddingVerticalValue) => {
  return {
    paddingHorizontal: paddingHorizontalValue,
    paddingVertical: paddingVerticalValue,
  };
};