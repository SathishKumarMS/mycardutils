const strings = {
  resultsFound: 'Results Found',
  discountOfferedByMerchants: 'Discount offered by each merchant(s)',
  kmsAway: 'Km(s) away',
  viewItems: 'View Item(s)',
  whoops: 'Whoops!',
  notFindAnything: "We couldn't find anything!",
};

export default strings;
