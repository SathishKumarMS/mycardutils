const colors = {
    primaryText: '#3A3246',
    lightText: '#98A5C4',
    barBackground: '#EEF2F8',
    shadow: '#333333',
}

export default colors;