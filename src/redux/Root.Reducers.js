import {combineReducers} from 'redux';
import myCardReducer from './reducers/MyCardReducer';

const appReducer = combineReducers({
  app: myCardReducer,
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
