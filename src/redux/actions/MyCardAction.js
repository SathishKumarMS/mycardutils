import * as Types from '../Root.Types';

export const availableItem = availableItem => {
  return {
    type: Types.AVAILABLE_ITEMS,
    payload: availableItem,
  };
};

export const loanerItem = loanerItem => {
  return {
    type: Types.LOANER_ITEMS,
    payload: loanerItem,
  };
};
