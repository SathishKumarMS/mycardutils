import * as Types from '../Root.Types';
import * as Static from '../../utils/Static';

const initialState = {
  loanerItem: [],
  availabeItem: Static.myCardData,
};

const myCardReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.AVAILABLE_ITEMS:
      return {...state, availabeItem: action.payload};
    default:
      return state;
  }
};

export default myCardReducer;
