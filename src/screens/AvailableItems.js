import React, {useCallback, useRef, useState} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  RefreshControl,
  I18nManager,
  ActivityIndicator,
} from 'react-native';
import {GestureHandlerRootView, Swipeable} from 'react-native-gesture-handler';
import {availableItem, loanerItem} from '../redux/actions/MyCardAction';
import Assets from '../assets/Index';
import {connect} from 'react-redux';
import Card from '../containers/Card';
import Colors from '../utils/Colors';
import NoResponse from '../components/NoResponse';
import Icon from 'react-native-vector-icons/Entypo';
import Styles from '../styles/AvailableItems.Styles';
import * as HelperStyles from '../utils/HelperStyles';

const AvailableItems = props => {
  // Ref Variables
  const swipeable = useRef();

  const [imageLoader, setImageLoader] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const ListItems = useCallback(() => {
    return props.availableItems.map((lol, index) => (
      <GestureHandlerRootView key={index}>
        <Swipeable
          enableTrackpadTwoFingerGesture={false}
          friction={2}
          ref={swipeable}
          renderRightActions={() => renderRightActions(lol)}
          onSwipeableOpen={() => handleSwipe(lol, index)}
          rightThreshold={40}>
          <Card disabled={true} containerStyle={Styles.cardContainer}>
            <View
              style={[
                HelperStyles.flex(0.35),
                HelperStyles.justifyContentCenteredView('center'),
              ]}>
              <Image
                onLoadStart={() => {
                  setImageLoader(true);
                }}
                onLoadEnd={() => {
                  setImageLoader(false);
                }}
                resizeMode={'cover'}
                source={{
                  uri: lol.image,
                }}
                style={[
                  HelperStyles.imageView(94, '100%'),
                  HelperStyles.justView('borderRadius', 4),
                ]}
              />
              {imageLoader && (
                <ActivityIndicator
                  size={'small'}
                  color={Colors.primary}
                  style={Styles.imageLoader}
                />
              )}
            </View>
            <View style={[HelperStyles.flex(0.65), HelperStyles.padding(8, 4)]}>
              <View style={Styles.availableNameContainer}>
                <View style={Styles.availableNameText}>
                  <Text
                    numberOfLines={2}
                    style={HelperStyles.textView(
                      14,
                      '700',
                      Colors.primaryText,
                      'left',
                      'none',
                    )}>
                    {lol.name}
                  </Text>
                </View>
              </View>
              <View style={Styles.brandContainer}>
                <View style={Styles.brandSubContainer}>
                  <View style={Styles.brandName}>
                    <Text
                      style={[
                        HelperStyles.textView(
                          12,
                          '600',
                          'gray',
                          'center',
                          'none',
                        ),
                        {marginRight: 4},
                      ]}>
                      {lol.brand}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={Styles.ratingAmountContainer}>
                <View style={Styles.subContainer}>
                  <Text
                    style={HelperStyles.textView(
                      14,
                      '800',
                      'black',
                      'center',
                      'none',
                    )}>
                    {`${'$'}${lol.prices} ${' '}`}
                  </Text>
                  <Text
                    style={HelperStyles.textView(
                      10,
                      '600',
                      'gray',
                      'center',
                      'none',
                    )}>
                    DAY
                  </Text>
                </View>
                <View style={Styles.subContainer}>
                  <Text
                    style={[
                      HelperStyles.textView(
                        10,
                        '600',
                        Colors.primaryText,
                        'center',
                        'none',
                      ),
                      {marginRight: 6},
                    ]}>
                    {lol.isCountry}
                  </Text>
                  <Text
                    style={[
                      HelperStyles.textView(
                        14,
                        '600',
                        'black',
                        'center',
                        'none',
                      ),
                    ]}>
                    {lol.size}
                  </Text>
                </View>
                <View style={Styles.subContainer}>
                  <Image
                    resizeMode={'contain'}
                    source={Assets.star}
                    style={[HelperStyles.imageView(16, 16), {marginRight: 4}]}
                  />
                  <Text
                    style={[
                      HelperStyles.textView(
                        12,
                        '600',
                        Colors.primaryText,
                        'center',
                        'none',
                      ),
                      {marginRight: 4},
                    ]}>
                    {lol.ratings}
                  </Text>
                </View>
                <View style={Styles.subContainer}>
                  <Icon
                    name="eye"
                    size={14}
                    color="gray"
                    style={{marginRight: 10}}
                  />
                  <Text
                    style={[
                      HelperStyles.textView(
                        14,
                        '600',
                        'black',
                        'center',
                        'none',
                      ),
                    ]}>
                    {lol.views}
                  </Text>
                </View>
              </View>
            </View>
          </Card>
        </Swipeable>
      </GestureHandlerRootView>
    ));
  }, [props.availableItems]);

  const handleSwipe = (lol, index) => {
    const loanerArray = props.loanerItems;
    const helperArray = props.availableItems;

    loanerArray.push(lol);

    props.updateLoanerItem(loanerArray);

    helperArray.splice(index, 1);

    props.updateAvailableItem([...helperArray]);
  };

  const renderRightActions = () => {
    return (
      <View
        style={[HelperStyles.justView('width', '100%'), Styles.swipeContainer]}>
        <View style={Styles.swipeSubContainer}>
          <Text
            style={HelperStyles.textView(16, '700', 'black', 'left', 'none')}>
            Removed
          </Text>
        </View>
      </View>
    );
  };

  return (
    <ScrollView
      contentContainerStyle={HelperStyles.flexGrow(1)}
      keyboardShouldPersistTaps={'handled'}
      showsVerticalScrollIndicator={false}
      refreshControl={
        <RefreshControl
          tintColor={'#EF3C47'}
          refreshing={refreshing}
          onRefresh={() => {
            setRefreshing(false);
          }}
        />
      }>
      <View style={[HelperStyles.flex(1), HelperStyles.margin(20, 16)]}>
        {Boolean(props.availableItems) && props.availableItems.length != 0 && (
          <View style={Styles.titleContainer}>
            <View style={Styles.titleLine}></View>
            <Text
              style={[
                HelperStyles.textView(20, '700', 'black', 'left', 'none'),
                {fontFamily: 'JosefinSans'},
              ]}>
              Available Items
            </Text>
          </View>
        )}
        {Boolean(props.availableItems) &&
        Array.isArray(props.availableItems) &&
        props.availableItems.length != 0 ? (
          <ListItems />
        ) : (
          <NoResponse />
        )}
      </View>
    </ScrollView>
  );
};

const mapStateToProps = state => {
  return {
    availableItems: state.app.availabeItem,
    loanerItems: state.app.loanerItem,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateLoanerItem: updateLoaner => {
      dispatch(loanerItem(updateLoaner));
    },

    updateAvailableItem: updateAvailableItem => {
      dispatch(availableItem(updateAvailableItem));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AvailableItems);
