import React, {useCallback, useState} from 'react';
import {View, Text, Image} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Assets from '../assets/Index';
import Colors from '../utils/Colors';
import TabBar from '../components/TapBar';
import LoanerItems from './LoanerItems';
import AvailableItems from './AvailableItems';
import Icon from 'react-native-vector-icons/FontAwesome';
import Store from '../redux/Store';
import * as Helpers from '../utils/Helpers';
import * as HelperStyles from '../utils/HelperStyles';
import {connect} from 'react-redux';

const MyCartUtils = props => {
  const TopTabs = createMaterialTopTabNavigator();
  const [currentRoute, setCurrentRoute] = useState('LoanerItems');
  const [loanerCount, setLoanerCount] = useState(0);
  const [availableCount, setAvailableCount] = useState(0);

  console.log(
    'kdjfkldsjfkjdskfjksdjfkjdslfjlksdjkfj',
    props.available,
    props.loaner,
  );

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      setLoanerCount(props.loaner);

      setAvailableCount(props.available);

      return () => {
        isFocus = false;
      };
    }, [props.loaner, props.available]),
  );

  const renderTabScreens = () => {
    return (
      <TopTabs.Navigator
        animationEnabled={true}
        initialRouteName={currentRoute}
        backBehavior={'history'}
        screenOptions={{swipeEnabled: false}}
        tabBar={props => (
          <TabBar
            {...props}
            getCurrentRoute={routeName => {
              setCurrentRoute(routeName);
            }}
            horizontalScrollView={false}
            tabBarItemStyle={HelperStyles.justView(
              'width',
              Helpers.windowWidth / 2,
            )}
            type={'outlined'}
          />
        )}
        style={{flex: 1}}>
        <TopTabs.Screen name="LoanerItems" component={LoanerItems} />
        <TopTabs.Screen name="AvailableItems" component={AvailableItems} />
      </TopTabs.Navigator>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={[
          {flex: 0.2, flexDirection: 'column', backgroundColor: 'white'},
          HelperStyles.margin(20, 0),
        ]}>
        <View style={{flex: 0.6, flexDirection: 'row'}}>
          <View
            style={{
              flex: 0.25,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <Image
              resizeMode={'contain'}
              source={Assets.profile}
              style={[HelperStyles.imageView(70, 70), {borderRadius: 70 / 2}]}
            />
          </View>
          <View
            style={{
              flex: 0.75,
              flexDirection: 'column',
            }}>
            <View
              style={{
                flex: 0.5,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginTop: 6,
              }}>
              <Text
                style={[
                  HelperStyles.textView(
                    18,
                    '600',
                    Colors.primaryText,
                    'left',
                    'none',
                  ),
                  {fontFamily: 'JosefinSans'},
                ]}>
                Maud Garrett
              </Text>
              <Text
                style={HelperStyles.textView(
                  12,
                  '600',
                  Colors.lightText,
                  'right',
                  'none',
                )}>
                Your Rating:
              </Text>
            </View>
            <View
              style={{
                flex: 0.5,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginBottom: 6,
              }}>
              <Text
                style={HelperStyles.textView(
                  14,
                  '600',
                  'gray',
                  'left',
                  'none',
                )}>
                #1234567
              </Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={[
                    HelperStyles.textView(
                      18,
                      '600',
                      Colors.lightText,
                      'right',
                      'none',
                    ),
                    {marginHorizontal: 6},
                  ]}>
                  4.5
                </Text>
                <Icon
                  name="star"
                  size={14}
                  color="yellow"
                  style={{marginHorizontal: 6}}
                />
                <Icon
                  name="star"
                  size={14}
                  color="yellow"
                  style={{marginHorizontal: 6}}
                />
                <Icon
                  name="star"
                  size={14}
                  color="yellow"
                  style={{marginHorizontal: 6}}
                />
                <Icon
                  name="star"
                  size={14}
                  color="yellow"
                  style={{marginHorizontal: 6}}
                />
                <Icon name="star" size={14} color="gray" />
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            flex: 0.4,
            flexDirection: 'row',
            // backgroundColor: 'yellow',
          }}>
          <View
            style={{
              justifyContent: 'space-around',
              alignItems: 'flex-start',
              marginRight: 18,
            }}>
            <Text
              style={[
                HelperStyles.textView(12, '600', 'gray', 'left', 'none'),
                {},
              ]}>
              Total Items:
            </Text>
            <Text
              style={[
                HelperStyles.textView(
                  18,
                  '700',
                  Colors.primaryText,
                  'left',
                  'none',
                ),
                {},
              ]}>
              14
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'space-around',
              alignItems: 'flex-start',
              marginRight: 18,
            }}>
            <Text
              style={[
                HelperStyles.textView(12, '600', 'gray', 'left', 'none'),
                {},
              ]}>
              Pending:
            </Text>
            <Text
              style={[
                HelperStyles.textView(
                  18,
                  '700',
                  Colors.primaryText,
                  'left',
                  'none',
                ),
                {},
              ]}>
              1
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'space-around',
              alignItems: 'flex-start',
              marginRight: 18,
            }}>
            <Text
              style={[
                HelperStyles.textView(12, '600', 'gray', 'left', 'none'),
                {},
              ]}>
              Loaned:
            </Text>
            <Text
              style={[
                HelperStyles.textView(
                  18,
                  '700',
                  Colors.primaryText,
                  'left',
                  'none',
                ),
                {},
              ]}>
              {loanerCount}
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'space-around',
              alignItems: 'flex-start',
            }}>
            <Text
              style={[
                HelperStyles.textView(12, '600', 'gray', 'left', 'none'),
                {},
              ]}>
              Available:
            </Text>
            <Text
              style={[
                HelperStyles.textView(
                  18,
                  '700',
                  Colors.primaryText,
                  'left',
                  'none',
                ),
                {},
              ]}>
              {availableCount}
            </Text>
          </View>
        </View>
      </View>
      <View style={{flex: 0.8}}>{renderTabScreens()}</View>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    loaner: state.app.loanerItem.length,
    available: state.app.availabeItem.length,
  };
};

export default connect(mapStateToProps, null)(MyCartUtils);
