import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import {GestureHandlerRootView, Swipeable} from 'react-native-gesture-handler';
import {availableItem, loanerItem} from '../redux/actions/MyCardAction';
import {connect} from 'react-redux';
import Assets from '../assets/Index';
import Card from '../containers/Card';
import Colors from '../utils/Colors';
import Styles from '../styles/LoanerItems.Styles';
import NoResponse from '../components/NoResponse';
import * as HelperStyles from '../utils/HelperStyles';

const LoanerItems = props => {
  // Ref Variables
  const swipeable = useRef();

  const [loanerdItems, setLoanerdItems] = useState(props.loaner);

  const [imageLoader, setImageLoader] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const ListItems = useCallback(() => {
    return loanerdItems.map((lol, index) => (
      <GestureHandlerRootView key={index}>
        <Swipeable
          enableTrackpadTwoFingerGesture={false}
          friction={2}
          ref={swipeable}
          renderLeftActions={() => renderLeftActions(lol)}
          onSwipeableOpen={() => handleSwipe(lol, index)}
          rightThreshold={40}>
          <Card disabled={true} containerStyle={Styles.cardContainer}>
            <View
              style={[
                HelperStyles.flex(0.35),
                HelperStyles.justifyContentCenteredView('center'),
              ]}>
              <Image
                onLoadStart={() => {
                  setImageLoader(true);
                }}
                onLoadEnd={() => {
                  setImageLoader(false);
                }}
                resizeMode={'cover'}
                source={{
                  uri: lol.image,
                }}
                style={[
                  HelperStyles.imageView(94, '100%'),
                  HelperStyles.justView('borderRadius', 4),
                ]}
              />
              {imageLoader && (
                <ActivityIndicator
                  size={'small'}
                  color={Colors.primary}
                  style={Styles.imageLoader}
                />
              )}
            </View>
            <View style={[HelperStyles.flex(0.65), HelperStyles.padding(8, 4)]}>
              <View style={Styles.loanerNameContainer}>
                <View style={Styles.loanerNameText}>
                  <Text
                    numberOfLines={2}
                    style={HelperStyles.textView(
                      14,
                      '700',
                      Colors.primaryText,
                      'left',
                      'none',
                    )}>
                    {lol.name}
                  </Text>
                </View>
              </View>
              <View style={Styles.loanerRatingDateContainer}>
                <View style={Styles.loanerRatingContainer}>
                  <View style={Styles.loanerRating}>
                    <View style={HelperStyles.margin(0, 0)}>
                      <Image
                        resizeMode={'contain'}
                        source={Assets.star}
                        style={HelperStyles.imageView(16, 16)}
                      />
                    </View>
                    <Text
                      style={[
                        HelperStyles.textView(
                          12,
                          '600',
                          Colors.primaryText,
                          'center',
                          'none',
                        ),
                        {marginRight: 4},
                      ]}>
                      {lol.ratings}
                    </Text>
                    <Text
                      style={[
                        HelperStyles.textView(
                          10,
                          '600',
                          Colors.primaryText,
                          'center',
                          'none',
                        ),
                        {marginRight: 4},
                      ]}>
                      {lol.isCountry}
                    </Text>
                    <Text
                      style={[
                        HelperStyles.textView(
                          14,
                          '600',
                          'black',
                          'center',
                          'none',
                        ),
                      ]}>
                      {lol.size}
                    </Text>
                  </View>
                </View>
                <View style={Styles.loanerDateContainer}>
                  <Text
                    style={HelperStyles.textView(
                      12,
                      '500',
                      'black',
                      'center',
                      'none',
                    )}>
                    {lol.date}
                  </Text>
                </View>
              </View>
              <View style={Styles.priceProfileContainer}>
                <View style={Styles.priceContainer}>
                  <Text
                    style={HelperStyles.textView(
                      14,
                      '800',
                      'black',
                      'center',
                      'none',
                    )}>
                    {`${'$'}${lol.prices} ${' '}`}
                    <Text
                      style={HelperStyles.textView(
                        10,
                        '600',
                        'gray',
                        'center',
                        'none',
                      )}>
                      TOTAL
                    </Text>
                  </Text>
                </View>
                <View style={Styles.profileContainer}>
                  <Text
                    style={HelperStyles.textView(
                      12,
                      '600',
                      'black',
                      'center',
                      'none',
                    )}>
                    {lol.itemProfileName}
                  </Text>
                  <View style={HelperStyles.margin(2, 0)}>
                    <Image
                      resizeMode={'contain'}
                      source={lol.itemProfile}
                      style={[
                        HelperStyles.imageView(24, 24),
                        HelperStyles.justView('tintColor', Colors.primary),
                        {borderRadius: 24 / 2},
                      ]}
                    />
                  </View>
                </View>
              </View>
            </View>
          </Card>
        </Swipeable>
      </GestureHandlerRootView>
    ));
  }, [loanerdItems]);

  useEffect(() => {
    let isFocus = true;
    setLoanerdItems(props.loaner);
    return () => {
      isFocus = false;
    };
  }, [loanerdItems]);

  const renderLeftActions = () => {
    return (
      <View
        style={[HelperStyles.justView('width', '100%'), Styles.swipeContainer]}>
        <View style={Styles.swipeSubContainer}>
          <Text
            style={HelperStyles.textView(16, '700', 'black', 'left', 'none')}>
            Removed
          </Text>
        </View>
      </View>
    );
  };

  const handleSwipe = (lol, index) => {
    const availableArray = [];
    const helperArray = loanerdItems;

    availableArray.push(lol);

    props.updateAvailableItem([...props.available, ...availableArray]);

    helperArray.splice(index, 1);

    setLoanerdItems([...helperArray]);

    props.updateLoanerItem([...helperArray]);
  };

  return (
    <ScrollView
      contentContainerStyle={HelperStyles.flexGrow(1)}
      keyboardShouldPersistTaps={'handled'}
      showsVerticalScrollIndicator={false}
      refreshControl={
        <RefreshControl
          tintColor={'#EF3C47'}
          refreshing={refreshing}
          onRefresh={() => {
            setRefreshing(false);
          }}
        />
      }>
      <View style={[HelperStyles.flex(1), HelperStyles.margin(20, 16)]}>
        {Boolean(loanerdItems) &&
          Array.isArray(loanerdItems) &&
          loanerdItems.length != 0 && (
            <View style={Styles.titleContainer}>
              <View style={Styles.titleLine}></View>
              <Text
                style={[
                  HelperStyles.textView(20, '700', 'black', 'left', 'none'),
                  {fontFamily: 'JosefinSans'},
                ]}>
                Loaned Items
              </Text>
            </View>
          )}
        {Boolean(loanerdItems) &&
        Array.isArray(loanerdItems) &&
        loanerdItems.length != 0 ? (
          <ListItems />
        ) : (
          <NoResponse />
        )}
      </View>
    </ScrollView>
  );
};

const mapStateToProps = state => {
  return {
    loaner: state.app.loanerItem,
    available: state.app.availabeItem,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateLoanerItem: updateLoaner => {
      dispatch(loanerItem(updateLoaner));
    },

    updateAvailableItem: updateAvailableItem => {
      dispatch(availableItem(updateAvailableItem));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoanerItems);
