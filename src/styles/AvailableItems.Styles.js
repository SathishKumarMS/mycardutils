import {StyleSheet, I18nManager} from 'react-native';

const AvailableItems = StyleSheet.create({
  cardContainer: {
    width: '99.75%',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginVertical: 8,
  },
  imageLoader: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
  },
  availableNameContainer: {
    flexDirection: 'row',
    height: 34,
    justifyContent: 'space-between',
  },
  availableNameText: {
    flex: 0.75,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  brandContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 4,
  },
  brandSubContainer: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  brandName: {
    flex: 0.25,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  ratingAmountContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 4,
  },
  subContainer: {
    flex: 0.25,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  swipeContainer: {
    height: '80%',
    flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  swipeSubContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 10,
  },
  titleLine: {
    width: 8,
    height: 30,
    backgroundColor: '#03452C',
    marginRight: 10,
  },
});

export default AvailableItems;
