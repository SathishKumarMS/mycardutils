import {StyleSheet} from 'react-native';
import * as Helpers from '../utils/Helpers';

const TabBar = StyleSheet.create({
  outLinedTabBarContainer: {
    flexDirection: 'row',
    marginVertical: 4,
  },
  outLinedContainer: {
    width: Helpers.windowWidth * 0.4875,
    height: Helpers.windowHeight * 0.05,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 3,
  },
});

export default TabBar;
