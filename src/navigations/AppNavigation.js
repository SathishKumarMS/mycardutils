import React from 'react';
import {View, Text} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MyCartUtils from '../screens/myCartUtils';

const AppNavigation = () => {
  // AppNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Group>
      <Stack.Screen name={'MyCartUtils'} component={MyCartUtils} />
    </Stack.Group>
  );
};

export default AppNavigation;
