import React from 'react';
import {View, Text} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AppNavigation from './AppNavigation';

const RootNavigation = () => {
  // RootNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Navigator
      initialRouteName="MyCartUtils"
      screenOptions={({navigation}) => ({
        headerBackTitleVisible: false,
        headerTitleAlign: 'center',
        headerShadowVisible: false,
        headerTitle: () => (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18, color: 'black', fontWeight:'600'}}>My Closet</Text>
          </View>
        ),
      })}>
      {AppNavigation()}
    </Stack.Navigator>
  );
};

export default RootNavigation;
