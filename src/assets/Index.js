const assets = {
  //Git
  noResponse: require('./gif/noResponse.gif'),
  profile: require('./Images/profile.jpg'),
  star: require('./Images/star.png'),
  subprofile: require('./Images/subProfile.jpeg')
};

export default assets;
