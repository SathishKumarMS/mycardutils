import React, {useEffect, useRef} from 'react';
import {Animated, ScrollView, TouchableOpacity, View} from 'react-native';
import Colors from '../utils/Colors';
import Styles from '../styles/TapBar';
import * as HelperStyles from '../utils/HelperStyles';

const TabBar = ({
  descriptors,
  getCurrentRoute = () => {},
  horizontalScrollView = false,
  navigation,
  state,
  tabBarActiveTintColor = Colors.primaryText,
  tabBarInactiveTintColor = Colors.lightText,
  tabBarItemTintColor = Colors.primary,
  tabBarItemStyle = null,
  tabBarLabelStyle = null,
  tabBarStyle = null,
  type = 'outlined',
}) => {
  // Ref Variables
  const scrollViewRef = useRef(null);

  // Other Variables
  let customtTabBarContainer = {},
    customTabBarItem = {};

  switch (type) {
    case 'outlined':
    default:
      customtTabBarContainer = [Styles.outLinedTabBarContainer];
      customTabBarItem = [Styles.outLinedContainer];
      tabBarActiveTintColor = 'black';
      tabBarInactiveTintColor = 'gray';
      break;
  }

  useEffect(() => {
    scrollViewRef.current &&
      scrollViewRef.current.scrollTo({
        x: state.index * 100,
        y: 0,
        animated: true,
      });
  }, [state.index]);

  const renderTopTabs = () => {
    return state.routes.map((route, index) => {
      const {options} = descriptors[route.key];

      const label = options.tabBarLabel || options.title || route.name;

      const isFocused = state.index === index;

      const onPress = () => {
        const event = navigation.emit({
          type: 'tabPress',
          target: route.key,
          canPreventDefault: true,
        });

        if (!isFocused && !event.defaultPrevented) {
          getCurrentRoute(route.name);

          navigation.navigate({name: route.name, merge: true});
        }
      };

      const onLongPress = () => {
        navigation.emit({
          type: 'tabLongPress',
          target: route.key,
        });
      };

      switch (type) {
        case 'outlined':
        default:
          customTabBarItem = [
            ...customTabBarItem,
            HelperStyles.justView(
              'borderColor',
              isFocused ? tabBarItemTintColor : Colors.barBackground,
            ),
          ];
          break;
      }

      return (
        <TouchableOpacity
          accessibilityRole="button"
          accessibilityState={isFocused ? {selected: true} : {}}
          accessibilityLabel={options.tabBarAccessibilityLabel}
          key={index}
          testID={options.tabBarTestID}
          onPress={onPress}
          onLongPress={onLongPress}
          style={HelperStyles.flex(1)}>
          <View style={[customTabBarItem, tabBarItemStyle]}>
            <View
              style={[
                HelperStyles.flexDirection('row'),
                HelperStyles.justifyContentCenteredView('center'),
              ]}>
              <View style={HelperStyles.justView('marginLeft', 8)}>
                <Animated.Text
                  style={[
                    HelperStyles.textView(
                      14,
                      '600',
                      isFocused
                        ? tabBarActiveTintColor
                        : tabBarInactiveTintColor,
                      'center',
                      'capitalize',
                    ),
                    tabBarLabelStyle,
                  ]}>
                  {label}
                </Animated.Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    });
  };

  return horizontalScrollView ? (
    <View>
      <ScrollView
        contentContainerStyle={[customtTabBarContainer, tabBarStyle]}
        horizontal={horizontalScrollView}
        ref={list => (scrollViewRef.current = list)}
        showsHorizontalScrollIndicator={false}>
        {renderTopTabs()}
      </ScrollView>
    </View>
  ) : (
    <View style={[customtTabBarContainer, tabBarStyle]}>{renderTopTabs()}</View>
  );
};

export default TabBar;
